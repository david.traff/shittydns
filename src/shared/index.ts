
import { readFile } from 'fs';
import { resolve } from 'path';
import { promisify } from 'util';

interface IConfig {
  server: string;
  heartbeatInterval: number;
  serverPort: number;
  clientPort: number;
}

interface IOptions {
  key: string;
  config?: IConfig;
}

// assume to be in dist/shared
const keyPath = '../../key';
const configPath = '../../config.json';
const readFileAsync = promisify(readFile);
const instance: IOptions = {
  key: '',
  config: undefined,
};

export const getConfig = async (): Promise<IConfig> => {
  if (!instance.config) {
    const path = resolve(__dirname, configPath);
    const data = await readFileAsync(path);

    instance.config = JSON.parse(data.toString());
  }

  return { ...instance.config } as IConfig;
};

export const getKey = async () => {
  if (!instance.key) {
    const data = await readFileAsync(keyPath);

    instance.key = data.toString();
  }

  return instance.key;
};

export const compareKey = async (key: string): Promise<boolean> => {
  if (!key) {
    return false;
  }

  const instanceKey = await getKey();

  return !!instanceKey && instanceKey === key;
};
