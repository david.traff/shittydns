import { Request, Response, NextFunction } from 'express';

export default (method: (request: Request, response: Response, nextFn?: NextFunction) => void) => {
  return (req: Request, res: Response, next?: NextFunction) => {
    try {
      method(req, res, next);
    } catch (e) {
      console.error(e);
      res.status(500).end('Something went wrong..');
    }
  };
};
