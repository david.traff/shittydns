import express, { Request, Response, NextFunction } from 'express';
import Server from 'http-proxy';
import handler from './handler';
import { compareKey, getConfig } from '../shared';

(async () => {
  const config = await getConfig();
  const proxy = Server.createProxyServer();
  let proxyTarget: string = '';

  const server = express();
  server.disable('x-powered-by');

  server.post('/heartbeat', handler(async (req, res) => {
    const { key } = req.query;
    const success = await compareKey(key);

    if (!success) {
      res.json({ success: false });
      return;
    }

    const address = req.headers['x-real-ip'] as string;
    if (address) {
      proxyTarget = address;

      console.log(new Date().toLocaleString(), 'Got a heartbeat from:', proxyTarget);
      res.json({ success: true });
    } else {
      res.json({ success: false });
    }
  }));

  server.use(handler((req, res, next) => {
    if (proxyTarget) {
      proxy.web(req, res, { target: `http://${proxyTarget}:${config.clientPort}` }, (err) => {
        console.error(err);
        res.status(500).end('Something went wrong');
      });
    } else if (next) {
      next();
    }
  }));

  server.listen(config.serverPort, () => {
    console.log(`ShittyDNS Listening on localhost:${config.serverPort}. Waiting for heartbeat..`);
  });
})();
