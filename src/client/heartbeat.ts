import request from 'request-promise-native';
import { getKey } from '../shared';

export interface IResponse {
  success: boolean;
}

export default async (uri: string): Promise<IResponse | undefined> => {
  try {
    const qs = {
      key: await getKey(),
    };

    const json = await request.post(uri, { qs });
    const data = JSON.parse(json) as IResponse;

    console.log(`Sent heartbeat to:`, uri, 'and got the response:', data);

    return data;
  } catch (e) {
    console.error(e);
    return undefined;
  }
};
