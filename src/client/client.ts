import heartbeat from './heartbeat';
import { getConfig } from '../shared';

const loop = async () => {
  const config = await getConfig();

  await heartbeat(config.server);
  setTimeout(loop, config.heartbeatInterval * 1000);
};

loop();
