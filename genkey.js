const crypto = require('crypto');
const fs = require('fs');

const path = './key';
const size = 256;

const rnd_key = crypto.randomBytes(size).toString('hex');

fs.writeFile(path, rnd_key, (err) => {
  if (err) {
    throw err;
  }

  console.log('Created the key.');
});
